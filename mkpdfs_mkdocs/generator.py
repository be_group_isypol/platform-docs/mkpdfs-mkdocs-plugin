import logging
import os
import sys
import pdb
from uuid import uuid4
from timeit import default_timer as timer

from weasyprint import HTML,urls, CSS
from bs4 import BeautifulSoup
from weasyprint.text.fonts import FontConfiguration

from mkpdfs_mkdocs.utils import gen_address
from mkpdfs_mkdocs.preprocessor import get_separate as prep_separate, get_combined as prep_combined

log = logging.getLogger(__name__)

class Generator(object):

    def __init__(self):
        self.config = None
        self.design = None
        self.mkdconfig = None
        self.nav = None
        self.logger = logging.getLogger('mkdocs.mkpdfs')
        self.generate = True
        self._articles = {}
        self._page_order = []
        self._base_urls = {}
        self._toc = None
        self.html = BeautifulSoup('<html><head></head>\
        <body></body></html>',
        'html.parser')
        self.dir = os.path.dirname(os.path.realpath(__file__))
        self.design = os.path.join(self.dir, 'design/report.css')

    def set_config(self, local, config):
        self.config = local;
        if self.config['design']:
            css_file = os.path.join(os.getcwd(), self.config['design'])
            if not os.path.isfile(css_file) :
                sys.exit('The file {} specified for design has not \
                been found.'.format(css_file))
            self.design = css_file
        self.title = config['site_name']
        self.config['copyright'] = 'CC-BY-SA\
        ' if not config['copyright'] else config['copyright']
        self.mkdconfig = config

    def write(self):
        if not self.generate:
            self.logger.log(msg='Unable to generate the PDF Version (See Mkpdfs doc)',
            level=logging.WARNING,)
            return
        self.gen_articles()
        font_config = FontConfiguration()
        css = self.add_css(font_config);
        pdf_path = os.path.join(self.mkdconfig['site_dir'],
        self.config['output_path'])
        os.makedirs(os.path.dirname(pdf_path), exist_ok=True)
        html = HTML(string=str(self.html)).write_pdf(pdf_path,
        font_config=font_config)
        self.logger.log(msg='The PDF version of the documentation has been generated.', level=logging.INFO,)

    def add_nav(self, nav):
        self.nav = nav
        for p in nav:
            self.addToOrder(p)

    def addToOrder(self, page):
        if page.is_page and page.meta != None and 'pdf' in page.meta and page.meta['pdf'] == False:
            print(page.meta)
            exit(1)
            return;
        if page.is_page :
            self._page_order.append(page.file.url)
        elif page.children:
            uuid = str(uuid4())
            self._page_order.append(uuid)
            title = self.html.new_tag('h1',
                id='{}-title'.format(uuid),
                **{'class': 'section_title'}
            )
            title.append(page.title)
            article = self.html.new_tag('article',
                id='{}'.format(uuid),
                **{'class': 'chapter'}
            )
            article.append(title)
            # Page with violet section title commented
            #self._articles[uuid] = article 
            for child in page.children:
                self.addToOrder(child)


    def remove_from_order(self, item):

        return

    def add_article(self, content, page, base_url):
        if not self.generate:
            return None
        self._base_urls[page.file.url] = base_url
        soup = BeautifulSoup(content, 'html.parser')
        url = page.url.split('.')[0]
        article = soup.find('article')
        if not article :
            article = self.html.new_tag('article')
            eld = soup.find('div', **{'role': 'main'})
            article.append(eld)
            article.div['class'] = article.div['role'] = None

        if not article:
            self.generate = False
            return None
        article = prep_combined(article, base_url, page.file.url)
        span = soup.new_tag('span')
        span['id'] = 'mkpdf-{}'.format(url)
        article.insert(0, span)
        if page.meta != None and 'pdf' in page.meta and page.meta['pdf'] == False:
            # print(page.meta)
            return self.get_path_to_pdf(page.file.dest_path)
        self._articles[page.file.url] = article
        return self.get_path_to_pdf(page.file.dest_path)

    def add_css(self, font_config):
        css_url = urls.path2url(self.design)
        self.html.head.clear()
        css_tag = BeautifulSoup(
            '<title>{}</title><link rel="stylesheet" \
            href="{}" type="text/css">'.
            format(self.title, css_url), 'html5lib')
        self.html.head.insert(0, css_tag)

    def get_path_to_pdf(self, start):
        pdf_split = os.path.split(self.config['output_path'])
        start_dir = os.path.split(start)[0]
        return os.path.join(os.path.relpath(pdf_split[0],
        start_dir), pdf_split[1])

    def add_tocs(self):
        title = self.html.new_tag('h1', id='doc-title')
        title.insert(0, self.config['toc_title'])
        self._toc = self.html.new_tag('article', id='contents')
        self._toc.insert(0, title)
        for n in self.nav:
            if n.is_page and n.meta != None and 'pdf' in n.meta \
            and n.meta['pdf'] == False:
                continue
            if hasattr(n, 'url'):
                # Skip toc generation for external links
                continue
            h3 = self.html.new_tag('h3')
            n.title = n.title.replace("&colon;", ":")
            h3.insert(0, n.title)
            self._toc.append(h3)
            if n.is_page :
                ptoc = self._gen_toc_page(n.file.url, n.toc)
                self._toc.append(ptoc)
            else :
                #self._gen_toc_section(n)
                self.gen_toc_section_recursive(n, [], 1)
        self.html.body.append(self._toc)

    def add_cover(self):
        a = self.html.new_tag('article', id='doc-cover')
        title = self.html.new_tag('h1', id='doc-title')
        title.insert(0, self.title)
        a.insert(0, title)
        a.append(gen_address(self.config))
        self.html.body.append(a)

    def gen_articles (self):
        self.add_cover()
        if self.config['toc_position'] == 'pre' :
            self.add_tocs()
        for url in self._page_order:
            if url in self._articles:
                self.html.body.append(self._articles[url])
        if self.config['toc_position'] == 'post' :
            self.add_tocs()

    def get_path_to_pdf(self, start):
        pdf_split = os.path.split(self.config['output_path'])
        start_dir = os.path.split(start)[0]
        return os.path.join(os.path.relpath(pdf_split[0], start_dir),
        pdf_split[1])


    def gen_toc_section_recursive(self, element, titleAr, level):
        #self.logger.log(msg='element: '+ element.title + ' ' + str(level) + ' ' + str(not element.is_page), level=logging.INFO)
        if (not element.is_page): # not is page
            sent = 1;
            level = level + 1
            element.title = element.title.replace("&colon;", ":")
            sectionStr = {
                "title": element.title,
                "level": level
            }
            titleAr.append(sectionStr)

            for p in element.children:
                count = len(titleAr)
                if(sent != 1):
                    titleAr = []
                #self.logger.log(msg='element: ' + p.title + ' ' + str(level), level=logging.INFO)
                self.gen_toc_section_recursive(p, titleAr, level)
                sent = 0

        else:
            #self.logger.log(msg='entro nel else: ' + str(level), level=logging.INFO)
            #self.logger.log(msg='element-foglia: ' + element.title + ' ' + str(titleAr), level=logging.INFO)
            stoc = self._gen_toc_for_section(element.file.url, element, 'sectionTab' + str(level))
            child = self.html.new_tag('div')
            for tt in titleAr:
                #self.logger.log(msg='section: ' + tt.get('title') + ', level: ' + str(tt.get('level')), level=logging.INFO)
                sectionData = self.setSectionTitleLink(element.file.url, tt.get('title'), 'sectionTab' + str(tt.get('level') - 1))
                child.append(sectionData)
                titleAr = []

            child.append(stoc)
            self._toc.append(child)

    '''
    def _gen_toc_section(self, section):
        sectionsAr = []
        if section.children:  # External Links do not have children
            for p1 in section.children:
                # level one
                if hasattr(p1, 'file'): # is page
                    stoc = self._gen_toc_for_section(p1.file.url, p1)
                    child = self.html.new_tag('div')
                    for section in sectionsAr:
                        sectionData = self.setSectionTitleLink(p1.file.url, section, 'sectionTab1')
                        child.append(sectionData)

                    sectionsAr = []
                    child.append(stoc)
                    self._toc.append(child)
                else: # is section
                    #  level two
                    sectionsAr.append(p1.title)
                    for p2 in p1.children:
                        if hasattr(p2, 'file'): # is page
                            stoc = self._gen_toc_for_section(p2.file.url, p2)
                            child = self.html.new_tag('div')
                            for section in sectionsAr:
                                sectionData = self.setSectionTitleLink(p2.file.url, section, 'sectionTab2')
                                child.append(sectionData)

                            sectionsAr = []
                            child.append(stoc)
                            self._toc.append(child)
                        else: # is section
                            #  level three
                            sectionsAr.append(p2.title)
                            for p3 in p2.children:
                                if hasattr(p3, 'file'): # is page
                                    stoc = self._gen_toc_for_section(p3.file.url, p3)
                                    child = self.html.new_tag('div')
                                    for section in sectionsAr:
                                        sectionData = self.setSectionTitleLink(p3.file.url, section, 'sectionTab3')
                                        child.append(sectionData)

                                    sectionsAr = []
                                    child.append(stoc)
                                    self._toc.append(child)
                                else: # is section
                                    #  level four
                                    level = 1
                                    sectionsAr.append(p3.title)
                                    #self.logger.log(msg='child: ' + str(p3.children), level=logging.INFO)
                                    for p4 in p3.children:
                                        if p4.is_page and p4.meta != None and 'pdf' \
                                            in p4.meta and p4.meta['pdf'] == False:
                                            continue
                                        if not hasattr(p4, 'file'):
                                            # Skip external links
                                            continue
                                        stoc = self._gen_toc_for_section(p4.file.url, p4)
                                        child = self.html.new_tag('div')
                                        for section in sectionsAr:
                                            self.logger.log( msg='section: ' + section + ', level: ' + str(level), level=logging.INFO)
                                            sectionData = self.setSectionTitleLink(p4.file.url, section, 'sectionTab' + str(level))
                                            level = level + 1
                                            child.append(sectionData)

                                        #if len(p3.children) == 1:
                                            sectionsAr = []

                                        child.append(stoc)
                                        self._toc.append(child)
    '''
    def _gen_children(self, url, children):
        ul = self.html.new_tag('ul')
        for child in children:
            a = self.html.new_tag('a', href=child.url)
            a.insert(0, child.title)
            li = self.html.new_tag('li')
            li.append(a)
            if child.children :
                sub = self._gen_children(url, child.children)
                li.append(sub)
            ul.append(li)
        return ul

    def _gen_toc_for_section(self, url, p, level):
        #pdb.set_trace()
        div = self.html.new_tag('div')
        menu = self.html.new_tag('div', id=p.title,  **{'class': level})
        h4 = self.html.new_tag('h4')
        urlid = url.split('.')[0]
        a = self.html.new_tag('a', href='#mkpdf-{}'.format(urlid))
        a.insert(0, p.title)
        h4.append(a)
        menu.append(h4)
        ul = self.html.new_tag('ul')
        for child in p.toc.items:
            a = self.html.new_tag('a', href=child.url)
            a.insert(0, child.title)
            li = self.html.new_tag('li')
            li.append(a)
            if child.title == p.title:
                li = self.html.new_tag('div');
            if child.children :
                sub = self._gen_children(url, child.children)
                li.append(sub)
            ul.append(li)
        if len(p.toc.items)>0:
            menu.append(ul)
        div.append(menu)
        div = prep_combined(div, self._base_urls[url], url)
        return div.find('div')

    def setSectionTitleLink(self, url, sectionTitle, sectionTab):
        div = self.html.new_tag('div')
        menu = self.html.new_tag('div',
                id='' + sectionTitle,
                **{'class': sectionTab})
        h4 = self.html.new_tag('H4')
        urlid = url.split('.')[0]
        a = self.html.new_tag('a', href='#mkpdf-{}'.format(urlid))
        #self.logger.log(msg='title: ' + sectionTitle + str(a), level=logging.INFO)
        a.insert(0, sectionTitle)
        h4.append(a)
        menu.append(h4)
        div.append(menu)
        div = prep_combined(div, self._base_urls[url], url)

        return div.find('div')

    def _gen_toc_page(self, url, toc):
        div = self.html.new_tag('div')
        menu = self.html.new_tag('ul')
        for item in toc.items:
            li = self.html.new_tag('li')
            a = self.html.new_tag('a', href=item.url)
            a.append(item.title)
            li.append(a)
            menu.append(li)
            if item.children :
                child = self._gen_children(url, item.children)
                menu.append(child)
        div.append(menu)
        div = prep_combined(div, self._base_urls[url], url)
        return div.find('ul')
